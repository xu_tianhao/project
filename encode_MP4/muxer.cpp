#include "muxer.h"

Muxer::Muxer()
{

}

int Muxer::Init(char* url)
{
    ret = avformat_alloc_output_context2(&fmt_ctx,NULL,NULL,url);
    if(ret!=0)
    {
        cout<<"avformat_alloc_output_context2 error"<<endl;
    }
    url_=url;
    return 0;

}

int Muxer::AddStream(AVCodecContext* codec_ctx)
{
    if(codec_ctx->codec->type==AVMEDIA_TYPE_VIDEO)
    {
        video_codec_ctx=codec_ctx;
     video_st_= avformat_new_stream(fmt_ctx,NULL);
    video_index=video_st_->index;
    avcodec_parameters_from_context(video_st_->codecpar,codec_ctx);
    }
    if(codec_ctx->codec->type==AVMEDIA_TYPE_AUDIO){
        audio_codec_ctx=codec_ctx;
        audio_st_= avformat_new_stream(fmt_ctx,NULL);
        avcodec_parameters_from_context(audio_st_->codecpar,codec_ctx);

            audio_index=audio_st_->index;
    }

}

int Muxer::SendHeader()
{
    ret=avformat_write_header(fmt_ctx,NULL);
    if(ret<0)return ret;
    return 0;
}

int Muxer::SendPacket(AVPacket *pkt,int time_base)
{
     AVRational dst_time_base;
    AVRational src_time_base;
    if(audio_st_&&pkt->stream_index==audio_index)
    {
        src_time_base=audio_codec_ctx->time_base;
        dst_time_base=audio_st_->time_base;
    }
    if(video_st_&&pkt->stream_index==video_index)
    {
        src_time_base=video_codec_ctx->time_base;
        dst_time_base=video_st_->time_base;
    }
    pkt->dts=av_rescale_q(pkt->dts,src_time_base,dst_time_base);
    pkt->pts=av_rescale_q(pkt->pts,src_time_base,dst_time_base);
    pkt->duration=av_rescale_q(pkt->duration,src_time_base,dst_time_base);
    ret=av_write_frame(fmt_ctx,pkt);
    cout<<pkt->pts<<" "<<pkt->dts<<" "<<pkt->size<<" "<<pkt->duration<<" "<<pkt->stream_index <<endl;

    av_packet_free(&pkt);
    if(ret<0)return ret;

    return 0;
}

int Muxer::SendTrailer()
{
    ret=av_write_trailer(fmt_ctx);
    if(ret<0)return ret;
    return 0;
}

int Muxer::Open()
{
    ret= avio_open(&fmt_ctx->pb,url_,AVIO_FLAG_WRITE);
    if(ret<0)return ret;
    return 0;
}
