#ifndef MUXER_H
#define MUXER_H

#include <iostream>
#include <vector>
using namespace  std;
extern "C"
{
#include "libavformat/avformat.h"
}

class Muxer
{
public:
    Muxer();
    int Init( char* url);

    int AddStream(AVCodecContext* codec_ctx);
    //写流
    int SendHeader();
    int SendPacket(AVPacket *pkt,int time_base);
    int SendTrailer();


    int Open();//avio open
    AVFormatContext* fmt_ctx;

    AVStream* audio_st_;
    AVStream* video_st_;
    AVCodecContext* audio_codec_ctx;
    AVCodecContext* video_codec_ctx;
    int audio_index;
    int video_index;
    char* url_;
    int ret;
};

#endif // MUXER_H
