#include "audio_encode.h"

audio_encode::audio_encode()
{


}

audio_encode::~audio_encode()
{
    if(codec_ctx)
        DeInit();
}

int audio_encode::Init(int channel_layouts,int sample_rate)
{

    codec=avcodec_find_encoder(AV_CODEC_ID_AAC);
    codec_ctx=avcodec_alloc_context3(codec);
    codec_ctx->channel_layout=channel_layouts_=channel_layouts;
    codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    codec_ctx->channels=channles_=av_get_channel_layout_nb_channels(channel_layouts);
    codec_ctx->sample_fmt=AV_SAMPLE_FMT_FLTP;
    codec_ctx->bit_rate=500*1024;
    codec_ctx->sample_rate=sample_rate_=sample_rate;

    avcodec_open2(codec_ctx,NULL,NULL);
    cout<<"audio_encode Init success"<<endl;
    return 0;
}

void audio_encode::DeInit()
{
    if(codec_ctx)
        avcodec_close(codec_ctx);
}

AVPacket* audio_encode::encode(Muxer* muxer,int pts,bool flag,int time_base,AVFrame* frame)
{
    if(flag)
    {
        frame=NULL;
    }
    else
    {
        pts = av_rescale_q(pts, AVRational{1, (int)time_base}, codec_ctx->time_base);
        frame->pts=pts;
    }

    ret = avcodec_send_frame(codec_ctx,frame);

    if(ret!=0)return NULL;


        packet=av_packet_alloc();
        ret=avcodec_receive_packet(codec_ctx,packet);
        if(ret==AVERROR(EAGAIN)||ret==AVERROR(AVERROR_EOF))
        {
            av_packet_free(&packet);
            return 0;
        }
        if(ret!=0)return NULL;
        packet->stream_index=muxer->audio_index;

    return packet;
}
