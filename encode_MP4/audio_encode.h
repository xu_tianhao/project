#ifndef AUDIO_ENCODE_H
#define AUDIO_ENCODE_H
#include <iostream>
#include <vector>
#include "muxer.h"
#include "audio_resampler.h"
using namespace  std;
extern "C"
{
#include "libavutil/imgutils.h"
#include "libavformat/avformat.h"
}
class audio_encode
{
public:
    audio_encode();
    ~audio_encode();
    int Init(int channel_layouts,int sample_rate);
    void DeInit();
    AVPacket* encode(Muxer* muxer,int pts,bool flag,int time_base,AVFrame* frame);
    AVPacket* packet;
    AVCodecContext* codec_ctx;
    AVCodec* codec;
    int channles_;
    int sample_rate_;
    int bit_rate_;
    int channel_layouts_;
    int ret;

};

#endif // AUDIO_ENCODE_H
