#include "video_encode.h"

video_encode::video_encode()
{

}

video_encode::~video_encode()
{
    if(codec_ctx)Deinit();
}



int video_encode::Init(int width,int height,int frame_rate)
{

    codec=avcodec_find_encoder(AV_CODEC_ID_H264);
    codec_ctx=avcodec_alloc_context3(codec);
    codec_ctx->width=width_=width;
      codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    codec_ctx->height=height_=height;
    codec_ctx->framerate=(AVRational){1,frame_rate};
    codec_ctx->gop_size=frame_rate;
    codec_ctx->max_b_frames=0;
    codec_ctx->time_base=(AVRational){1,1000000};
    codec_ctx->bit_rate=5000*1024;
    codec_ctx->pix_fmt=AV_PIX_FMT_YUV420P;
    AVDictionary* dict=NULL;
    av_dict_set(&dict,"preset","ultrafast",0);
    av_dict_set(&dict,"tune","zerolatency",0);
    avcodec_open2(codec_ctx,NULL,&dict);

    cout<<"video_endcode Init success"<<endl;
    return 0;
}

void video_encode::Deinit()
{
    if(codec_ctx)
        avcodec_close(codec_ctx);
}

AVPacket* video_encode::encode(Muxer* muxer,vector<AVPacket *> &pkts,int pts,uint8_t* yuv_buff,bool flag,int time_base)
{
    if(flag)
    {
        frame=NULL;
    }
    else
    {
        pts = av_rescale_q(pts, AVRational{1, (int)time_base}, codec_ctx->time_base);
        frame=av_frame_alloc();
        frame->width=width_;
        frame->height=height_;
        frame->format=codec_ctx->pix_fmt;
        av_frame_get_buffer(frame,0);
        frame->pts=pts;
        av_image_fill_arrays(frame->data,frame->linesize,
                             yuv_buff,(enum AVPixelFormat)(frame->format),
                             frame->width,frame->height,1);
    }

    ret = avcodec_send_frame(codec_ctx,frame);
    av_frame_free(&frame);
    if(ret!=0)return NULL;

//    while(1)
    {   packet=av_packet_alloc();
        ret=avcodec_receive_packet(codec_ctx,packet);
        if(ret==AVERROR(EAGAIN)||ret==AVERROR(AVERROR_EOF))

        {
            av_packet_free(&packet);
            return NULL;
        }
        if(ret!=0)return NULL;

        packet->stream_index=muxer->video_index;
//        pkts.push_back(packet);
    }
    return packet;
}

