#ifndef VIDIO_ENCODE_H
#define VIDIO_ENCODE_H
#include <iostream>
#include <vector>
#include "muxer.h"
using namespace  std;
extern "C"
{
#include "libavutil/imgutils.h"
#include "libavformat/avformat.h"
}

class video_encode
{
public:
    video_encode();
    ~video_encode();
    int Init(int width,int height,int frame_rate);
    void Deinit();

    AVPacket* encode(Muxer* muxer,vector<AVPacket *> &pkts,int pts,uint8_t* yuv_buff,bool flag,int time_base);
    AVPacket* packet;
    AVFrame*frame;
    AVCodec* codec;
    AVCodecContext* codec_ctx;
    int width_;
    int height_;
    int bit_rate_;
    int ret;


};

#endif // VIDIO_ENCODE_H
