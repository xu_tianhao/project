#include <iostream>
#include "audio_encode.h"
#include "video_encode.h"
#include "audio_resampler.h"
#include "muxer.h"
extern "C"{
#include "libavutil/imgutils.h"
}
using namespace std;
#define TIME_BAST 1000000
#define FRAME_RATE 25
#define WIDTH 720
#define HEIGTH 576
#define SAMPLE_RATE  44100
#define SRC_SAMPLE_FORMAT  AV_SAMPLE_FMT_S16
int main(int argc,char* argv[])
{
    char* pcm_path=argv[1];
    char*yuv_path=argv[2];
    char* out_path=argv[3];
    FILE* pcm_file=fopen(pcm_path,"rb");
    FILE* yuv_file=fopen(yuv_path,"rb");

    audio_encode audio_encoder;
    audio_encoder.Init(AV_CH_LAYOUT_STEREO,SAMPLE_RATE);
    video_encode video_encoder;
    video_encoder.Init(WIDTH,HEIGTH,FRAME_RATE);
    Muxer muxer;
    muxer.Init(out_path);
    muxer.Open();
    muxer.AddStream(audio_encoder.codec_ctx);
    muxer.AddStream(video_encoder.codec_ctx);
    AudioResampler audio_resampler;
    audio_resampler.InitFromS16ToFLTP(2,SAMPLE_RATE,2,SAMPLE_RATE);


    muxer.SendHeader();
    vector<AVPacket*> pkts;
    int yuv_frame_size=av_image_get_buffer_size((enum AVPixelFormat)video_encoder.codec_ctx->pix_fmt,video_encoder.width_,video_encoder.height_,1);
    uint8_t yuv_buff[yuv_frame_size];
    memset(yuv_buff,0,yuv_frame_size);
    cout<<av_get_bytes_per_sample((enum AVSampleFormat)SRC_SAMPLE_FORMAT)<<endl;
    int pcm_frame_size=av_samples_get_buffer_size(NULL,audio_encoder.channles_,audio_encoder.codec_ctx->frame_size,(enum AVSampleFormat)SRC_SAMPLE_FORMAT,1);
    uint8_t pcm_buff[pcm_frame_size];
    memset(pcm_buff,0,pcm_frame_size);
    double video_frame_duration=1.0/FRAME_RATE*TIME_BAST;
    double audio_frame_duration=1.0*audio_encoder.codec_ctx->frame_size/SAMPLE_RATE*TIME_BAST;
    double video_pts=0;
    double audio_pts=0;
    int video_finish=0;
    int audio_finish=0;
    AVPacket* pkt=NULL;
    while(!video_finish||!audio_finish)
    {

        printf("apts:%0.0lf vpts:%0.0lf\n", audio_pts/1000, video_pts/1000);
        if(!video_finish&&video_pts<audio_pts||(audio_finish&&!video_finish))
        {
            int size=fread(yuv_buff,1,yuv_frame_size,yuv_file);
            if(size<yuv_frame_size)
            {
                video_finish=1;
                continue;
            }
            pkt=video_encoder.encode(&muxer,pkts,video_pts,yuv_buff,false,TIME_BAST);
            video_pts+=video_frame_duration;
        }
        else if(!audio_finish)
        {
            int size=fread(pcm_buff,1,pcm_frame_size,pcm_file);
            if(size<pcm_frame_size)
            {
                audio_finish=1;
                continue;
            }
            AVFrame* frame=AllocFltpPcmFrame(2,audio_encoder.codec_ctx->frame_size);
            audio_resampler.ResampleFromS16ToFLTP(pcm_buff,frame);
            pkt=audio_encoder.encode(&muxer,audio_pts,false,TIME_BAST,frame);
            audio_pts+=audio_frame_duration;
            FreePcmFrame(frame);
        }
        if(pkt)
            muxer.SendPacket(pkt,TIME_BAST);
    }
    pkt=video_encoder.encode(&muxer,pkts,video_pts,yuv_buff,true,TIME_BAST);
    if(pkt)
        muxer.SendPacket(pkt,TIME_BAST);
    pkt=audio_encoder.encode(&muxer,audio_pts,true,TIME_BAST,NULL);
    if(pkt)
        muxer.SendPacket(pkt,TIME_BAST);


    muxer.SendTrailer();
    return 0;
}
